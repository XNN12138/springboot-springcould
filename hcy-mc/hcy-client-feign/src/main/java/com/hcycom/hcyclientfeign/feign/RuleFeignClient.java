package com.hcycom.hcyclientfeign.feign;

import com.hcycom.hcymodel.pojo.RuleTest;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("RuleServer")
public interface RuleFeignClient {

    /**
     * 使用feign坑较多
     * 坑一：GetMapping在这里不能使用，会报错，必须使用RequestMapping+Get
     * 坑二：PathVarable注解中必须有value  否则会报错
     * 坑三：只要参数是复杂对象，即使使用了GET方法，feign也会使用盘post请求进行处理
     * 坑四：在pom文件中，版本管理本身的版本必须是Brixton.SR5以上的版本，否则会报： Attribute 'value' in annotation [org.springframework.cloud.netflix.feign.FeignClient] must be declared as an @AliasFor [serviceId], not [name].
     */

    /**
     *
     * @param ruleTest
     * @return
     */

    @RequestMapping(method = RequestMethod.POST,value="test/select")
    List<RuleTest> feignTest(@RequestBody RuleTest ruleTest);
}
