package com.hcycom.hcyclientfeign.controller;

import com.hcycom.hcyclientfeign.feign.RuleFeignClient;
import com.hcycom.hcymodel.pojo.RuleTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 负载均衡测试类：
 * 默认是服务器的均衡使用
 */
@RestController
@RequestMapping(value ="/test",produces = "application/json")
public class FeignController {

    @Autowired
    RuleFeignClient ruleFeignClient;

    @RequestMapping(method = RequestMethod.POST,value="/select")
    List<RuleTest> feignTest(@RequestBody RuleTest ruleTest){
        return ruleFeignClient.feignTest(ruleTest);
    }
}
