package com.hcycom.hcyzuul;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableZuulProxy
public class HcyZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(HcyZuulApplication.class, args);
	}
}
