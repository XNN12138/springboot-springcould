package com.hcycom.hcymybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;

/**
 * 批量插入处理
 * @param <T>
 */
public interface ListMapper<T> {
    /**
     * 添加通用的批量插入的方法
     * 批量插入，支持批量插入的数据库可以使用，例如MySQL,H2等，另外该接口限制实体包含`id`
     * 必须在方法上添加注解。查询使用SelectProvider，插入使用@InsertProvider，更新使用UpdateProvider，删除使用DeleteProvider。不同的Provider就相当于xml中不同的节点，如<select>,<insert>,<update>,<delete>
     * type必须设置为实际执行方法的HasqldbProvider.class,method必须设置为"dynamicSQL"
     * 通用Mapper处理的时候会根据type反射HasqldbProvider查找方法，而Mybatis的处理机制要求method必须是type类中只有一个入参，且返回值为String的方法。"dynamicSQL"方法定义在MapperTemplate中
     * @param recordList
     * @return
     */
    @Options(useGeneratedKeys = false, keyProperty = "id")      //@Options注解中默认设置的主键对应的字段名为id、
    @InsertProvider(type = ListProvider.class, method = "dynamicSQL")       //自定义泛型的批量插入语句
    int insertLists(List<T> recordList);
}
