package com.hcycom.hcymybatis.mapper;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 基础实体类
 */
public class BasicEntity {
    //主键
    @Id
    @Column(name = "id")                                        //只针对数据库中id主键
    @GeneratedValue(generator = "UUID")             //生成主键的形式为UUID
    private String id;

    //分页页码
    @Transient
    private Integer page = 1;
    //分页每页显示数目
    @Transient
    private Integer pageSize = 10;
    //排序方式
    @Transient
    @JsonIgnore
    private String orderSql;

    public String getOrderSql() {
        return orderSql;
    }

    public void setOrderSql(String orderSql) {
        this.orderSql = orderSql;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * 生成UUID作为数据库主键
     *
     * @return
     */
    public static String getUUID() {
        String s = UUID.randomUUID().toString();
        // 去掉“-”符号
        return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
    }

    @Override
    public String toString() {
        return "BaseEntity [id=" + id + "]";
    }
}
