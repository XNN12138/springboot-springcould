package com.hcycom.hcymybatis.mapper;

import com.github.pagehelper.PageInfo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BasicService<T extends BasicEntity> {

    /**
     * 根据实体类不为null的字段进行查询,条件全部使用=号and条件
     *
     * @param < T
	 *            extend T>
     */
    public List<T> select(T record);

    public List<T> select(T record, String order);

    /**
     * 根据实体类不为null的字段查询总数,条件全部使用=号and条件
     *
     * @param < T
	 *            extend T>
     */
    public int selectCount(T record);

    /**
     * 根据主键进行查询,必须保证结果唯一 单个字段做主键时,可以直接写主键的值 联合主键时,key可以是实体类,也可以是Map
     *
     * @param < T
	 *            extend T>
     */
    public T selectByPrimaryKey(Object key);

    public T selectOne(T record);

    /**
     * 插入一条数据,只插入不为null的字段,不会影响有默认值的字段
     * 支持Oracle序列,UUID,类似Mysql的INDENTITY自动增长(自动回写)
     * 优先使用传入的参数值,参数值空时,才会使用序列、UUID,自动增长
     *
     * @param < T
	 *            extend T>
     */
    @Transactional
    public int insertSelective(T record);

    /**
     * 保存或者更新，根据传入id主键是不是null来确认
     *
     * @param record
     * @return 影响行数
     */
    @Transactional
    public int insertOrUpdate(T record);

    /**
     * 根据实体类不为null的字段进行查询,条件全部使用=号and条件
     *
     * @param < T
	 *            extend T>
     */
    @Transactional
    public int delete(T key);

    /**
     * 通过主键进行删除,这里最多只会删除一条数据 单个字段做主键时,可以直接写主键的值 联合主键时,key可以是实体类,也可以是Map
     *
     * @param < T
	 *            extend T>
     */
    @Transactional
    public int deleteByPrimaryKey(Object key);

    /**
     * 根据主键进行更新 只会更新不是null的数据
     *
     * @param < T
	 *            extend T>
     */
    @Transactional
    public int updateByPrimaryKeySelective(T record);

    /**
     * 根据主键进行更新 null值会被更新
     *
     * @param < T
	 *            extend T>
     */
    @Transactional
    public int updateByPrimaryKey(T record);

    @Transactional
    int insertList(List<T> list);

    public PageInfo<T> selectPage(int pageNum, int pageSize, T record);

    /**
     *
     * (组合查询带分页)<BR>
     * 方法名：selectPages<BR>
     * 创建人：wjf <BR>
     * 时间：2017年7月1日-下午1:09:02 <BR>
     *
     * @param pageNum
     * @param pageSize
     * @param record
     * @param orderStr
     * @return PageInfo<T><BR>
     * @exception <BR>
     * @since 1.0.0
     */
    public PageInfo<T> selectPages(int pageNum, int pageSize, T record, String orderStr);

    public PageInfo<T> selectPage(int pageNum, int pageSize, T record, String orderStr);
}
