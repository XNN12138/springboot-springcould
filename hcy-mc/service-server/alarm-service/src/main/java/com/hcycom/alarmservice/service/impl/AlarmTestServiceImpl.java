package com.hcycom.alarmservice.service.impl;


import com.hcycom.alarmservice.service.AlarmTestService;
import com.hcycom.hcymybatis.mapper.BaseServiceImplement;
import org.springframework.stereotype.Service;

@Service
public class AlarmTestServiceImpl extends BaseServiceImplement<AlarmTest> implements AlarmTestService {
}
