package com.hcycom.alarmservice.controller;



import com.hcycom.alarmservice.service.AlarmTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value ="/test",produces = "application/json")
public class AlarmTestController {

    @Autowired
    AlarmTestService testService;

    @RequestMapping(value = "/test")
    public String test(@RequestBody AlarmTest test){
        return "helloword";
    }

    @RequestMapping(value = "/inset", produces = "application/json")
    public int insertTest(@RequestBody AlarmTest test){
        return this.testService.insertSelective(test);
    }

    @RequestMapping(value = "/select", produces = "application/json")
    public List<AlarmTest> select(@RequestBody AlarmTest test){
        return this.testService.select(test);
    }
}

