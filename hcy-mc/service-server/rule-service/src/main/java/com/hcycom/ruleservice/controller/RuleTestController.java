package com.hcycom.ruleservice.controller;

import com.hcycom.hcymodel.pojo.RuleTest;
import com.hcycom.ruleservice.service.RuleTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value ="/test",produces = "application/json")
public class RuleTestController {
    @Autowired
    RuleTestService testService;

    @RequestMapping(value = "/test")
    public String test(@RequestBody RuleTest test){
        return "helloword";
    }

    @RequestMapping(value = "/inset", produces = "application/json")
    public int insertTest(@RequestBody RuleTest test){
        return this.testService.insertSelective(test);
    }

    @RequestMapping(value = "/select", method = RequestMethod.POST,produces = "application/json")
    public List<RuleTest> select(@RequestBody RuleTest test){
        return this.testService.select(test);
    }
}
