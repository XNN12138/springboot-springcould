package com.hcycom.ruleservice.service.impl;

import com.hcycom.hcymodel.pojo.RuleTest;
import com.hcycom.hcymybatis.mapper.BaseServiceImplement;
import com.hcycom.ruleservice.service.RuleTestService;
import org.springframework.stereotype.Service;

@Service
public class RuleTestServiceImpl extends BaseServiceImplement<RuleTest> implements RuleTestService {
}
