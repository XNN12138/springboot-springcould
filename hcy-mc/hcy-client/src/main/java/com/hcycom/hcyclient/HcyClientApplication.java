package com.hcycom.hcyclient;

import com.hcycom.hcyclient.config.ExcludeFormCompnentScan;
import defindribbon.TestConfigurationOne;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.client.RestTemplate;


@EnableDiscoveryClient
@SpringBootApplication
@EnableCircuitBreaker				//添加断路器
//@RibbonClient(name="alarmServer",configuration = TestConfigurationOne.class)		//对alarmServer服务器实行自定义的负载均衡
//@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION,value = ExcludeFormCompnentScan.class)})      //自定义负载均衡2：自定义ExcludeFormCompnentScan注解，可以实现在application可扫描的包下面自定义配置
public class HcyClientApplication {

	/**
	 * TODO 自定义RibbonClient方式：
	 * 方式1：
	 * 		在application扫描包之外使用Configuration注解实现
	 * 	方式2：
	 * 		使用自定义主角+ComponentScan的方式
	 * 	方式3：在配置文件中使用
	 *
	 * 	就优先级而言：最高的是配置文件配置    其次是在类中配置   最后是使用默认的
	 */

	/**
	 * TODO 断路器的配置方式：
	 * 1.加入starter-hystrix依赖
	 * 2.在启动类上加@EnableCircuitBreaker注解
	 * 3.在想要使用断路器的请求方法上加@HystrixCommand(fallbackMethod = "defaultStores")
	 */

	/**
	 * 负载均衡
	 * @return
	 */
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(HcyClientApplication.class, args);
	}
}
