package defindribbon;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义robbin配置方式1：
 * 此类必须写在启动类扫描报的外面，不然此配置会覆盖掉其他不想进行此项配置的所有类
 */
@Configuration
public class TestConfigurationOne {
    @Autowired
    IClientConfig iClientConfig;

    @Bean
    public IRule ribbonRule(){
        return new RandomRule();
    }
}
