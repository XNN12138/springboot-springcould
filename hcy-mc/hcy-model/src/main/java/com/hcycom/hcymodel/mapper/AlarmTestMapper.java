package com.hcycom.hcymodel.mapper;

import com.hcycom.hcymodel.pojo.AlarmTest;
import com.hcycom.hcymybatis.mapper.MyMapper;

public interface AlarmTestMapper extends MyMapper<AlarmTest> {
}