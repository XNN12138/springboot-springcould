package com.hcycom.hcymodel.pojo;

import com.hcycom.hcymybatis.mapper.BasicEntity;

import javax.persistence.*;

@Table(name = "rule_test")
public class RuleTest extends BasicEntity{

    /**
     * 生产时间
     */
    @Column(name = "both_data")
    private String bothData;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 产品价格
     */
    private Integer price;

    /**
     * 产品类别
     */
    private String type;


    /**
     * 获取生产时间
     *
     * @return both_data - 生产时间
     */
    public String getBothData() {
        return bothData;
    }

    /**
     * 设置生产时间
     *
     * @param bothData 生产时间
     */
    public void setBothData(String bothData) {
        this.bothData = bothData;
    }

    /**
     * 获取产品名称
     *
     * @return name - 产品名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置产品名称
     *
     * @param name 产品名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取产品价格
     *
     * @return price - 产品价格
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * 设置产品价格
     *
     * @param price 产品价格
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * 获取产品类别
     *
     * @return type - 产品类别
     */
    public String getType() {
        return type;
    }

    /**
     * 设置产品类别
     *
     * @param type 产品类别
     */
    public void setType(String type) {
        this.type = type;
    }
}