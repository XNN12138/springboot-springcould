package com.hcycom.hcymodel.mapper;

import com.hcycom.hcymodel.pojo.RuleTest;
import com.hcycom.hcymybatis.mapper.MyMapper;

public interface RuleTestMapper extends MyMapper<RuleTest> {
}