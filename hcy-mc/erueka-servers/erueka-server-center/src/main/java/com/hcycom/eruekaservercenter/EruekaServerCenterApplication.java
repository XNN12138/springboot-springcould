package com.hcycom.eruekaservercenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EruekaServerCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(EruekaServerCenterApplication.class, args);
	}
}
